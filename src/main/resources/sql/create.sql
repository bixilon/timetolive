CREATE TABLE IF NOT EXISTS `stats` (
  `uuid` varchar(36) NOT NULL,
  `name` varchar(16) NOT NULL,
  `games` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `wins` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `kills` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `total_lifetime_gained` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
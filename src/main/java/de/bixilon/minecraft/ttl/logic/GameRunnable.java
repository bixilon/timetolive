package de.bixilon.minecraft.ttl.logic;

import de.bixilon.minecraft.ttl.TTL;
import de.bixilon.minecraft.ttl.data.PlayerData;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameRunnable implements Runnable {
    private static int m_timer = -1; // lobby timer
    private static int m_wait_for_more_players_countdown;
    private static List<Integer> m_countdown_notify = new ArrayList<>(Arrays.asList(90, 60, 30, 10, 5, 4, 3, 2, 1));

    public static void checkLobbyTimerStart() {
        // check if countdown started
        if (m_timer == -1) {
            // no? check if enough players are online
            if (TTL.getInstance().m_config.getConfig().getInt("config.lobby.players-to-start") <= TTL.getInstance().getServer().getOnlinePlayers().size()) {
                // start timer
                m_timer = TTL.getInstance().m_config.getConfig().getInt("config.lobby.lobby-timer");
                return;
            }
        }

        // check if server is full
        if (TTL.getInstance().getServer().getOnlinePlayers().size() < TTL.getInstance().m_config.getConfig().getInt("config.general.max-players")) {
            // not full
            return;
        }
        // if lobby timer < full timer
        if (m_timer > TTL.getInstance().m_config.getConfig().getInt("config.lobby.lobby-timer-full")) {
            // set lobby timer to full timer
            m_timer = TTL.getInstance().m_config.getConfig().getInt("config.lobby.lobby-timer-full");
        }

    }

    public static void checkLobbyTimerStop() {
        // check if timer started
        if (m_timer == -1) {
            // nope, return
            return;
        }
        //check if enough players are online
        if (TTL.getInstance().m_config.getConfig().getInt("config.lobby.players-to-start") <= TTL.getInstance().getServer().getOnlinePlayers().size() - 1) {
            // yes, enough
            return;
        }

        // nope, stop timer
        m_timer = -1;
        TTL.getInstance().getServer().broadcastMessage(TTL.getInstance().getString("language.general.not-enough-players"));
        // clear xp level
        for (Player c_p : TTL.getInstance().getServer().getOnlinePlayers()) {
            c_p.setLevel(0);
        }
    }

    public static void shutdown() {
        // starts shutdown timer
        m_timer = 20;
        TTL.getInstance().getServer().broadcastMessage(String.format(TTL.getInstance().getString("language.general.server-stops-in"), 20));
    }

    public static boolean checkWin() {
        PlayerData c_alive = null;
        // get all players
        for (Player c_p : TTL.getInstance().getServer().getOnlinePlayers()) {
            PlayerData c_pd = TTL.getInstance().m_data.getPlayerData(c_p.getUniqueId());
            if (c_pd == null) {
                // spectator
                continue;
            }
            // if not dead and no player was written to c_alive, this player is the first one
            if (!c_pd.isDead()) {
                if (c_alive == null) {
                    // nobody before me...
                    c_alive = c_pd;
                } else {
                    // whoops, an other player is still alive, returning
                    return false;
                }
            }
        }
        if (c_alive == null) {
            // should not happen
            return true;
        }
        // only 1 player alive, ending game

        TTL.getInstance().setGameStatus(TTL.GameStatus.ENDED);
        TTL.getInstance().getServer().broadcastMessage(String.format(TTL.getInstance().getString("language.general.won-message"), c_alive.getName()));
        c_alive.addWin();
        return true;
    }

    @Override
    public void run() {
        switch (TTL.getInstance().m_status) {
            case WAITING:
                if (m_timer != -1) {
                    // timer started

                    // check if game should start
                    if (m_timer == 0) {
                        TTL.getInstance().getServer().broadcastMessage(TTL.getInstance().getString("language.general.game-starts"));
                        m_timer = TTL.getInstance().m_config.getConfig().getInt("config.general.protection-time");
                        TTL.getInstance().getServer().broadcastMessage(String.format(TTL.getInstance().getString("language.general.protection-time-ends-in"), m_timer));
                        // clear xp level
                        for (Player c_p : TTL.getInstance().getServer().getOnlinePlayers()) {
                            c_p.setLevel(0);
                        }
                        TTL.getInstance().setGameStatus(TTL.GameStatus.PROTECTION_TIME);
                        return;
                    }

                    // broadcast import messages
                    if (m_countdown_notify.contains(m_timer)) {
                        TTL.getInstance().getServer().broadcastMessage(String.format(TTL.getInstance().getString("language.general.game-starts-in"), m_timer));
                    }
                    // set xp Level
                    for (Player c_p : TTL.getInstance().getServer().getOnlinePlayers()) {
                        c_p.setLevel(m_timer);
                    }
                    m_timer--;


                } else {
                    // do we need to say the players, that we are waiting for more players?
                    if (m_wait_for_more_players_countdown == 10) {
                        //yes, send message waiting for more players
                        TTL.getInstance().getServer().broadcastMessage(TTL.getInstance().getString("language.general.waiting-for-players"));
                        // reset counter
                        m_wait_for_more_players_countdown = -1;
                    } else {
                        // no, increase counter to wait up to 10 seconds
                        m_wait_for_more_players_countdown++;
                    }
                }
                break;
            case PROTECTION_TIME:
                // update blocks
                TTL.getInstance().m_data.tickAllBlocks();
                if (m_timer != 0) {
                    if (m_countdown_notify.contains(m_timer)) {
                        TTL.getInstance().getServer().broadcastMessage(String.format(TTL.getInstance().getString("language.general.protection-time-ends-in"), m_timer));
                    }
                    m_timer--;
                    break;
                }
                // end protection time
                TTL.getInstance().getServer().broadcastMessage(TTL.getInstance().getString("language.general.protection-time-ends"));
                TTL.getInstance().setGameStatus(TTL.GameStatus.RUNNING);
                break;
            case RUNNING:
                // update all blocks
                TTL.getInstance().m_data.tickAllBlocks();
                // remove 1 second of lifetime from every player
                for (PlayerData c_pd : TTL.getInstance().m_data.getPlayerData().values()) {
                    if (c_pd.getPlayer() == null) {
                        // player left the game
                        c_pd.saveToMySQL();
                        TTL.getInstance().m_data.getPlayerData().remove(c_pd.getUUID());
                        continue;
                    }
                    if (c_pd.isDead()) {
                        continue;
                    }
                    c_pd.removeTime(1);
                    c_pd.refreshScoreboard();
                    if (c_pd.getLifeTime() == 0) {
                        // sorry, no time any more
                        c_pd.getPlayer().setHealth(0.0D);
                    }
                }
                break;
            case ENDED:
                if (m_timer != -1) {
                    // timer started
                    // check if server should stop
                    if (m_timer == 0) {
                        TTL.getInstance().getServer().broadcastMessage(TTL.getInstance().getString("language.general.server-stops"));
                        for (Player c_p : TTL.getInstance().getServer().getOnlinePlayers()) {
                            c_p.kickPlayer(TTL.getInstance().getString("language.general.server-stops"));
                        }
                        m_timer = -1;
                        // clear xp level
                        for (Player c_p : TTL.getInstance().getServer().getOnlinePlayers()) {
                            c_p.setLevel(0);
                        }
                        TTL.getInstance().getServer().shutdown();
                        return;
                    }

                    // broadcast import messages
                    if (m_countdown_notify.contains(m_timer)) {
                        TTL.getInstance().getServer().broadcastMessage(String.format(TTL.getInstance().getString("language.general.server-stops-in"), m_timer));
                    }
                    // set xp Level
                    for (Player c_p : TTL.getInstance().getServer().getOnlinePlayers()) {
                        c_p.setLevel(m_timer);
                    }
                    m_timer--;
                }
                break;
        }
    }
}

package de.bixilon.minecraft.ttl.logic;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.BlockPosition;
import de.bixilon.minecraft.ttl.TTL;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;

public class CustomBlock {
    private static int m_last_block_entity_id = 10000;
    private final Location m_location;
    private final int m_entity_id; // for block break
    private int m_seconds;

    public CustomBlock(Location c_location) {
        m_location = c_location;
        m_entity_id = m_last_block_entity_id;
        m_last_block_entity_id++;
    }

    public Location getLocation() {
        return m_location;
    }

    public boolean tick() {
        if (m_seconds == 9) {
            return true;
        }
        PacketContainer c_packet = TTL.getInstance().m_proto.createPacket(PacketType.Play.Server.BLOCK_BREAK_ANIMATION);
        c_packet.getBlockPositionModifier().write(0, new BlockPosition(m_location.getBlockX(), m_location.getBlockY(), m_location.getBlockZ())); //position
        c_packet.getIntegers().write(0, m_entity_id);
        c_packet.getIntegers().write(1, m_seconds); //stage
        for (Player c_p : TTL.getInstance().getServer().getOnlinePlayers()) {
            try {
                TTL.getInstance().m_proto.sendServerPacket(c_p, c_packet);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
        m_seconds++;
        return false;
    }
}

package de.bixilon.minecraft.ttl;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import de.bixilon.minecraft.ttl.commands.Shop;
import de.bixilon.minecraft.ttl.data.Configuration;
import de.bixilon.minecraft.ttl.data.Data;
import de.bixilon.minecraft.ttl.data.PlayerData;
import de.bixilon.minecraft.ttl.data.ShopType;
import de.bixilon.minecraft.ttl.events.PlayerEvents;
import de.bixilon.minecraft.ttl.events.ProtectEvent;
import de.bixilon.minecraft.ttl.events.ShopEvents;
import de.bixilon.minecraft.ttl.logic.GameRunnable;
import de.bixilon.minecraft.ttl.mysql.Driver;
import de.bixilon.minecraft.ttl.mysql.MySQL;
import org.bukkit.Location;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.io.IOException;
import java.util.Objects;

public final class TTL extends JavaPlugin {
    public static TTL m_instance; // singleton interface
    public Configuration m_config;
    public Configuration m_lang;
    public Data m_data;
    public MySQL m_mysql;
    public boolean m_debug;
    private Scoreboard m_ghost_sb;
    private Team m_ghost_team;
    public ProtocolManager m_proto;

    public GameStatus m_status;

    @Override
    public void onEnable() {
        m_instance = this;

        // load config
        try {
            m_config = new Configuration("config.yml");
        } catch (IOException e) {
            getServer().getConsoleSender().sendMessage(String.format("§c[TTL] Critical Error: could not read config file: %s", e.getMessage()));
            e.printStackTrace();
            getServer().shutdown();
            return;
        }
        getServer().getConsoleSender().sendMessage(String.format("[TTL] Loaded config file (version %s)", m_config.getConfig().getInt("version")));


        // load language
        try {
            m_lang = new Configuration("de_DE.yml");
        } catch (IOException e) {
            getServer().getConsoleSender().sendMessage(String.format("§c[TTL] Critical Error: could not read language file: %s", e.getMessage()));
            e.printStackTrace();
            getServer().shutdown();
            return;
        }
        getServer().getConsoleSender().sendMessage(String.format("[TTL] Loaded language file (version %s)", m_lang.getConfig().getInt("version")));

        // mysql
        getServer().getConsoleSender().sendMessage("[TTL] Connecting to MySQL server...");
        m_mysql = new MySQL(new Driver(m_config.getConfig().getString("mysql-host"), 3306, m_config.getConfig().getString("mysql-database"), m_config.getConfig().getString("mysql-user"), m_config.getConfig().getString("mysql-password")));
        if (m_mysql.isConnected()) {
            getServer().getConsoleSender().sendMessage("[TTL] Connected to MySQL server!");
        } else {
            getServer().getConsoleSender().sendMessage("[TTL] Connection to MySQL Server failed!");
            getServer().shutdown();
            return;
        }
        m_mysql.init();

        // events and commands
        registerEvents();
        registerCommands();

        // check if debug is active
        m_debug = m_config.getConfig().getBoolean("debug");

        // get all data like lobby location, ...
        m_data = new Data();
        // lobby location
        m_data.setLobbyLocation(new Location(
                getServer().getWorld(Objects.requireNonNull(m_config.getConfig().getString("locations.lobby.world"))),
                m_config.getConfig().getDouble("locations.lobby.x"),
                m_config.getConfig().getDouble("locations.lobby.y"),
                m_config.getConfig().getDouble("locations.lobby.z"),
                (float) m_config.getConfig().getDouble("locations.lobby.yaw"),
                (float) m_config.getConfig().getDouble("locations.lobby.pitch")));

        // spectator location
        m_data.setSpectatorLocation(new Location(
                getServer().getWorld(Objects.requireNonNull(m_config.getConfig().getString("locations.spectator.world"))),
                m_config.getConfig().getDouble("locations.spectator.x"),
                m_config.getConfig().getDouble("locations.spectator.y"),
                m_config.getConfig().getDouble("locations.spectator.z"),
                (float) m_config.getConfig().getDouble("locations.spectator.yaw"),
                (float) m_config.getConfig().getDouble("locations.spectator.pitch")));

        // register game "logic" ticker
        getServer().getScheduler().scheduleSyncRepeatingTask(getInstance(), new GameRunnable(), 0, 20);

        // register scoreboard spectator team
        m_ghost_sb = getServer().getScoreboardManager().getNewScoreboard();
        // register team
        m_ghost_team = m_ghost_sb.registerNewTeam("team");
        m_ghost_team.setAllowFriendlyFire(false);
        m_ghost_team.setCanSeeFriendlyInvisibles(true);
        m_ghost_team.setDisplayName("Spectators");

        // init shop
        ShopType.init();

        // protocol lib
        m_proto = ProtocolLibrary.getProtocolManager();

        // start game
        m_status = GameStatus.WAITING;

    }

    @Override
    public void onDisable() {
        // closing mysql connection
        m_mysql.disconnect();

        // break all blocks to restore the world
        m_data.breakAllBlocks();
    }

    public static TTL getInstance() {
        return m_instance;
    }

    public void registerEvents() {
        getServer().getPluginManager().registerEvents(new PlayerEvents(), getInstance());
        getServer().getPluginManager().registerEvents(new ProtectEvent(), getInstance());
        getServer().getPluginManager().registerEvents(new ShopEvents(), getInstance());
    }

    public void registerCommands() {
        getCommand("shop").setExecutor(new Shop());
    }

    public static void removeAllBadEntities() {
        // thanks https://www.spigotmc.org/threads/clear-items-on-ground.256468
        TTL.getInstance().m_data.getSpectatorLocation().getWorld().getEntities().stream().filter(Item.class::isInstance).forEach(Entity::remove);
        TTL.getInstance().m_data.getSpectatorLocation().getWorld().getEntities().stream().filter(Arrow.class::isInstance).forEach(Entity::remove);
    }

    public enum GameStatus {
        WAITING,
        PROTECTION_TIME,
        RUNNING,
        ENDED
    }

    public void setSpectator(Player c_player) {
        PlayerData c_player_data = m_data.getPlayerData(c_player.getUniqueId());
        m_ghost_team.addEntry(c_player.getName());
        c_player.setScoreboard(m_ghost_sb);

        c_player.setAllowFlight(true);
        for (Player p : TTL.getInstance().getServer().getOnlinePlayers()) {
            if (p == c_player.getPlayer()) {
                continue;
            }
            if (c_player_data != null) {
                if (c_player_data.isDead() && m_data.getPlayerData(p.getUniqueId()).isDead()) {
                    p.showPlayer(TTL.getInstance(), c_player);
                } else {
                    p.hidePlayer(TTL.getInstance(), c_player);
                }
            } else {
                p.hidePlayer(TTL.getInstance(), c_player);
            }

        }
    }

    public String getString(String c_path) {
        return m_lang.getConfig().getString(c_path);
    }

    public void setGameStatus(GameStatus c_gamestatus) {
        if (c_gamestatus == m_status) {
            // status already set
            return;
        }
        switch (c_gamestatus) {
            case ENDED:
                for (Player c_player : getServer().getOnlinePlayers()) {
                    //teleport to lobby
                    c_player.teleport(m_data.getLobbyLocation());
                    // disable spectators
                    for (Player c_to_show_player : getServer().getOnlinePlayers()) {
                        c_player.showPlayer(TTL.getInstance(), c_to_show_player);
                    }
                    c_player.removePotionEffect(PotionEffectType.INVISIBILITY);

                    // clear inventory
                    c_player.getInventory().clear();

                    // remove score board
                    c_player.getScoreboard().clearSlot(DisplaySlot.SIDEBAR);

                    PlayerData c_pd = m_data.getPlayerData(c_player.getUniqueId());
                    if (c_pd == null) {
                        // joined as spectator
                        continue;
                    }
                    c_player.sendMessage(getString("language.end.stats-of-this-round"));
                    c_player.sendMessage(String.format(getString("language.end.kills"), c_pd.getKills()));
                    c_player.sendMessage(String.format(getString("language.end.time-gained"), c_pd.getLifeTimeGained()));
                }
                // destroy all blocks placed by placers
                m_data.breakAllBlocks();

                // remove all entities (dropped items and arrows)
                removeAllBadEntities();

                // restart server
                GameRunnable.shutdown();
                break;
            case PROTECTION_TIME:

                // remove all items on ground
                removeAllBadEntities();

                // teleport all players to a random (predefined) location on the map
                int i = 0;
                for (Player c_player : getServer().getOnlinePlayers()) {
                    c_player.teleport(new Location(
                            getServer().getWorld(Objects.requireNonNull(m_config.getConfig().getString(String.format("locations.spawn%s.world", i)))),
                            m_config.getConfig().getDouble(String.format("locations.spawn%s.x", i)),
                            m_config.getConfig().getDouble(String.format("locations.spawn%s.y", i)),
                            m_config.getConfig().getDouble(String.format("locations.spawn%s.z", i)),
                            (float) m_config.getConfig().getDouble(String.format("locations.spawn%s.yaw", i)),
                            (float) m_config.getConfig().getDouble(String.format("locations.spawn%s.pitch", i))));
                    m_data.generatePlayerData(c_player);
                    i++;
                }
                break;


        }
        m_status = c_gamestatus;
    }
}

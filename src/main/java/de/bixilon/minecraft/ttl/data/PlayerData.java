package de.bixilon.minecraft.ttl.data;

import de.bixilon.minecraft.ttl.TTL;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.util.UUID;

public class PlayerData {
    private final UUID m_uuid;
    private final String m_name;
    private final Player m_player;

    Score m_score_kills;
    Score m_score_time;

    private int m_time_left;
    private int m_time_gained;
    private int m_kills;
    private boolean m_won = false;
    private boolean m_is_dead = false;
    private boolean m_saved = false;

    public PlayerData(Player c_player) {
        m_uuid = c_player.getUniqueId();
        m_name = c_player.getName();
        m_player = c_player;
        if (TTL.getInstance().m_status != TTL.GameStatus.WAITING) {
            // game has already started (or ended)
            m_is_dead = true;
            return;
        }

        // get default time
        m_time_left = TTL.getInstance().m_config.getConfig().getInt("config.times.default-time");

        Scoreboard c_scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective c_objective = c_scoreboard.registerNewObjective(c_player.getName(), c_player.getName(), TTL.getInstance().getString("language.scoreboard.header"));
        c_objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        m_score_kills = c_objective.getScore(TTL.getInstance().getString("language.scoreboard.kills"));
        m_score_kills.setScore(0);
        m_score_time = c_objective.getScore(TTL.getInstance().getString("language.scoreboard.time"));
        m_score_time.setScore(m_time_left);
        c_player.setScoreboard(c_scoreboard);
        c_player.setLevel(m_time_left);
    }

    public Player getPlayer() {
        return m_player;
    }

    public UUID getUUID() {
        return m_uuid;
    }

    public String getName() {
        return m_name;
    }

    public void saveToMySQL() {
        // data was already saved
        if (m_saved) {
            return;
        }
        TTL.getInstance().m_mysql.saveData(this);
        m_saved = true;
    }

    public void addKill() {
        m_kills++;
        refreshScoreboard();
    }

    public void addTime(int c_seconds) {
        m_time_left += c_seconds;
        m_time_gained += c_seconds;
        setTimeLevel();
    }

    public void setTimeLevel() {
        if (TTL.getInstance().m_status == TTL.GameStatus.RUNNING || TTL.getInstance().m_status == TTL.GameStatus.PROTECTION_TIME) {
            getPlayer().setLevel(m_time_left);
        }
    }

    public void removeTime(int c_seconds) {
        m_time_left -= c_seconds;
        setTimeLevel();
    }

    public void refreshScoreboard() {
        m_score_time.setScore(m_time_left);
        m_score_kills.setScore(m_kills);
    }

    public int getLifeTime() {
        return m_time_left;
    }

    public void addGame() {
        m_is_dead = true;
        m_player.getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
        saveToMySQL();
    }

    public boolean isDead() {
        return m_is_dead;
    }

    public void addWin() {
        m_won = true;
        saveToMySQL();
    }

    public int getLifeTimeGained() {
        return m_time_gained;
    }

    public boolean buy(int c_seconds) {
        if (m_time_left <= c_seconds - 1) { // -1 avoids buying stuff and having 0 time to life
            return false;
        }
        removeTime(c_seconds);
        refreshScoreboard();
        return true;
    }

    public int getKills() {
        return m_kills;
    }

    public boolean hasWon() {
        return m_won;
    }
}

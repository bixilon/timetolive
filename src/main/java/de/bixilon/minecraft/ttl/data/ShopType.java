package de.bixilon.minecraft.ttl.data;

import de.bixilon.minecraft.ttl.TTL;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collections;

public class ShopType {
    public static Inventory CATEGORIES;
    public static Inventory FOOD;
    public static Inventory SWORD;
    public static Inventory ARMOR;
    public static Inventory BOW;
    public static Inventory SPECIAL;
    public static Inventory BLOCKS;
    public static Inventory POTIONS;

    public static ItemStack m_background_glass;
    public static ItemStack m_close;
    public static ItemStack m_back;

    public static void init() {
        // not a good way to to this, but it is so for now!

        m_background_glass = new ItemStack(Material.PURPLE_STAINED_GLASS_PANE);


        m_close = new ItemStack(Material.RED_STAINED_GLASS_PANE, 1);
        ItemMeta m_close_meta = m_close.getItemMeta();
        m_close_meta.setDisplayName(TTL.getInstance().getString("language.shop.close"));
        m_close.setItemMeta(m_close_meta);

        m_back = new ItemStack(Material.RED_STAINED_GLASS_PANE, 1);
        ItemMeta m_back_meta = m_back.getItemMeta();
        m_back_meta.setDisplayName(TTL.getInstance().getString("language.shop.back"));
        m_back.setItemMeta(m_back_meta);

        initCategories();
        initFood();
        initSword();
        initBow();
        initBlocks();


        //ToDo
        ARMOR = TTL.getInstance().getServer().createInventory(null, 27, "§6Rüstung");
        SPECIAL = TTL.getInstance().getServer().createInventory(null, 27, "§6Special");
        POTIONS = TTL.getInstance().getServer().createInventory(null, 27, "§6Tränke");
    }

    private static void initBlocks() {
        BLOCKS = TTL.getInstance().getServer().createInventory(null, 27, TTL.getInstance().getString("language.shop.blocks.header"));

        ItemStack c_glass = new ItemStack(Material.GLASS, 16);
        ItemMeta c_glass_meta = c_glass.getItemMeta();
        c_glass_meta.setDisplayName(TTL.getInstance().getString("language.shop.blocks.glass"));
        c_glass_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 8))));
        c_glass.setItemMeta(c_glass_meta);

        ItemStack c_wool = new ItemStack(Material.WHITE_WOOL, 16);
        ItemMeta c_wool_meta = c_wool.getItemMeta();
        c_wool_meta.setDisplayName(TTL.getInstance().getString("language.shop.blocks.wool"));
        c_wool_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 10))));
        c_wool.setItemMeta(c_wool_meta);


        ItemStack c_obsidian = new ItemStack(Material.OBSIDIAN, 16);
        ItemMeta c_obsidian_meta = c_obsidian.getItemMeta();
        c_obsidian_meta.setDisplayName(TTL.getInstance().getString("language.shop.blocks.obsidian"));
        c_obsidian_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 16))));
        c_obsidian.setItemMeta(c_obsidian_meta);

        BLOCKS.setItem(0, m_background_glass);
        BLOCKS.setItem(1, m_background_glass);
        BLOCKS.setItem(2, m_background_glass);
        BLOCKS.setItem(3, m_background_glass);
        BLOCKS.setItem(4, m_background_glass);
        BLOCKS.setItem(5, m_background_glass);
        BLOCKS.setItem(6, m_background_glass);
        BLOCKS.setItem(7, m_background_glass);
        BLOCKS.setItem(8, m_back);
        BLOCKS.setItem(9, m_background_glass);
        BLOCKS.setItem(10, m_background_glass);
        BLOCKS.setItem(11, m_background_glass);
        BLOCKS.setItem(12, c_glass);
        BLOCKS.setItem(13, c_wool);
        BLOCKS.setItem(14, c_obsidian);
        BLOCKS.setItem(15, m_background_glass);
        BLOCKS.setItem(16, m_background_glass);
        BLOCKS.setItem(17, m_background_glass);
        BLOCKS.setItem(18, m_background_glass);
        BLOCKS.setItem(19, m_background_glass);
        BLOCKS.setItem(20, m_background_glass);
        BLOCKS.setItem(21, m_background_glass);
        BLOCKS.setItem(22, m_background_glass);
        BLOCKS.setItem(23, m_background_glass);
        BLOCKS.setItem(24, m_background_glass);
        BLOCKS.setItem(25, m_background_glass);
        BLOCKS.setItem(26, m_background_glass);
    }

    private static void initBow() {
        BOW = TTL.getInstance().getServer().createInventory(null, 27, TTL.getInstance().getString("language.shop.bow.header"));

        ItemStack c_bow_1 = new ItemStack(Material.BOW, 1);
        ItemMeta c_bow_1_meta = c_bow_1.getItemMeta();
        c_bow_1_meta.setDisplayName(TTL.getInstance().getString("language.shop.bow.normal"));
        c_bow_1_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 10))));
        c_bow_1.setItemMeta(c_bow_1_meta);

        ItemStack c_bow_2 = new ItemStack(Material.BOW, 1);
        ItemMeta c_bow_2_meta = c_bow_2.getItemMeta();
        c_bow_2_meta.setDisplayName(TTL.getInstance().getString("language.shop.bow.power-1"));
        c_bow_2_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 20))));
        c_bow_2.setItemMeta(c_bow_2_meta);
        c_bow_2.addEnchantment(Enchantment.ARROW_DAMAGE, 1);

        ItemStack c_bow_3 = new ItemStack(Material.BOW, 1);
        ItemMeta c_bow_3_meta = c_bow_3.getItemMeta();
        c_bow_3_meta.setDisplayName(TTL.getInstance().getString("language.shop.bow.power-3"));
        c_bow_3_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 75))));
        c_bow_3.setItemMeta(c_bow_3_meta);
        c_bow_3.addEnchantment(Enchantment.ARROW_DAMAGE, 3);

        ItemStack c_bow_3_infinity = new ItemStack(Material.BOW, 1);
        ItemMeta c_bow_3_infinity_meta = c_bow_3_infinity.getItemMeta();
        c_bow_3_infinity_meta.setDisplayName(TTL.getInstance().getString("language.shop.bow.power-3i"));
        c_bow_3_infinity_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 120))));
        c_bow_3_infinity.setItemMeta(c_bow_3_infinity_meta);
        c_bow_3_infinity.addEnchantment(Enchantment.ARROW_DAMAGE, 3);
        c_bow_3_infinity.addEnchantment(Enchantment.ARROW_INFINITE, 1);

        ItemStack c_bow_4 = new ItemStack(Material.BOW, 1);
        ItemMeta c_bow_4_meta = c_bow_4.getItemMeta();
        c_bow_4_meta.setDisplayName(TTL.getInstance().getString("language.shop.bow.power-5f"));
        c_bow_4_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 150))));
        c_bow_4.setItemMeta(c_bow_4_meta);
        c_bow_4.addEnchantment(Enchantment.ARROW_DAMAGE, 5);
        c_bow_4.addEnchantment(Enchantment.ARROW_FIRE, 1);

        ItemStack c_bow_5 = new ItemStack(Material.BOW, 1);
        ItemMeta c_bow_5_meta = c_bow_5.getItemMeta();
        c_bow_5_meta.setDisplayName(TTL.getInstance().getString("language.shop.bow.power-5fi"));
        c_bow_5_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 200))));
        c_bow_5.setItemMeta(c_bow_5_meta);
        c_bow_5.addEnchantment(Enchantment.ARROW_DAMAGE, 5);
        c_bow_5.addEnchantment(Enchantment.ARROW_FIRE, 1);
        c_bow_5.addEnchantment(Enchantment.ARROW_INFINITE, 1);

        ItemStack c_arrow = new ItemStack(Material.ARROW, 8);
        ItemMeta c_arrow_meta = c_arrow.getItemMeta();
        c_arrow_meta.setDisplayName(TTL.getInstance().getString("language.shop.bow.arrow"));
        c_arrow_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 10))));
        c_arrow.setItemMeta(c_arrow_meta);


        BOW.setItem(0, m_background_glass);
        BOW.setItem(1, m_background_glass);
        BOW.setItem(2, m_background_glass);
        BOW.setItem(3, m_background_glass);
        BOW.setItem(4, m_background_glass);
        BOW.setItem(5, m_background_glass);
        BOW.setItem(6, m_background_glass);
        BOW.setItem(7, m_background_glass);
        BOW.setItem(8, m_back);
        BOW.setItem(9, m_background_glass);
        BOW.setItem(10, c_bow_1);
        BOW.setItem(11, c_bow_2);
        BOW.setItem(12, c_bow_3);
        BOW.setItem(13, c_bow_3_infinity);
        BOW.setItem(14, c_bow_4);
        BOW.setItem(15, c_bow_5);
        BOW.setItem(16, c_arrow);
        BOW.setItem(17, m_background_glass);
        BOW.setItem(18, m_background_glass);
        BOW.setItem(19, m_background_glass);
        BOW.setItem(20, m_background_glass);
        BOW.setItem(21, m_background_glass);
        BOW.setItem(22, m_background_glass);
        BOW.setItem(23, m_background_glass);
        BOW.setItem(24, m_background_glass);
        BOW.setItem(25, m_background_glass);
        BOW.setItem(26, m_background_glass);
    }

    private static void initSword() {
        SWORD = TTL.getInstance().getServer().createInventory(null, 27, TTL.getInstance().getString("language.shop.sword.header"));

        ItemStack c_wood_sword = new ItemStack(Material.WOODEN_SWORD, 1);
        ItemMeta c_wood_sword_meta = c_wood_sword.getItemMeta();
        c_wood_sword_meta.setDisplayName(TTL.getInstance().getString("language.shop.sword.wood"));
        c_wood_sword_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 10))));
        c_wood_sword.setItemMeta(c_wood_sword_meta);
        c_wood_sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);

        ItemStack c_stone_sword = new ItemStack(Material.STONE_SWORD, 1);
        ItemMeta c_stone_sword_meta = c_stone_sword.getItemMeta();
        c_stone_sword_meta.setDisplayName(TTL.getInstance().getString("language.shop.sword.stone"));
        c_stone_sword_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 20))));
        c_stone_sword.setItemMeta(c_stone_sword_meta);
        c_stone_sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);

        ItemStack c_iron_sword = new ItemStack(Material.IRON_SWORD, 1);
        ItemMeta c_iron_sword_meta = c_iron_sword.getItemMeta();
        c_iron_sword_meta.setDisplayName(TTL.getInstance().getString("language.shop.sword.iron"));
        c_iron_sword_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 50))));
        c_iron_sword.setItemMeta(c_iron_sword_meta);
        c_iron_sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);

        ItemStack c_diamond_sword = new ItemStack(Material.DIAMOND_SWORD, 1);
        ItemMeta c_diamond_sword_meta = c_diamond_sword.getItemMeta();
        c_diamond_sword_meta.setDisplayName(TTL.getInstance().getString("language.shop.sword.diamond"));
        c_diamond_sword_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 100))));
        c_diamond_sword.setItemMeta(c_diamond_sword_meta);
        c_diamond_sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);


        ItemStack c_diamond_sword_fire = new ItemStack(Material.DIAMOND_SWORD, 1);
        ItemMeta c_diamond_sword_fire_meta = c_diamond_sword_fire.getItemMeta();
        c_diamond_sword_fire_meta.setDisplayName(TTL.getInstance().getString("language.shop.sword.diamond-f"));
        c_diamond_sword_fire_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 150))));
        c_diamond_sword_fire.setItemMeta(c_diamond_sword_fire_meta);
        c_diamond_sword_fire.addEnchantment(Enchantment.DAMAGE_ALL, 1);
        c_diamond_sword_fire.addEnchantment(Enchantment.FIRE_ASPECT, 1);


        SWORD.setItem(0, m_background_glass);
        SWORD.setItem(1, m_background_glass);
        SWORD.setItem(2, m_background_glass);
        SWORD.setItem(3, m_background_glass);
        SWORD.setItem(4, m_background_glass);
        SWORD.setItem(5, m_background_glass);
        SWORD.setItem(6, m_background_glass);
        SWORD.setItem(7, m_background_glass);
        SWORD.setItem(8, m_back);
        SWORD.setItem(9, m_background_glass);
        SWORD.setItem(10, m_background_glass);
        SWORD.setItem(11, c_wood_sword);
        SWORD.setItem(12, c_stone_sword);
        SWORD.setItem(13, c_iron_sword);
        SWORD.setItem(14, c_diamond_sword);
        SWORD.setItem(15, c_diamond_sword_fire);
        SWORD.setItem(16, m_background_glass);
        SWORD.setItem(17, m_background_glass);
        SWORD.setItem(18, m_background_glass);
        SWORD.setItem(19, m_background_glass);
        SWORD.setItem(20, m_background_glass);
        SWORD.setItem(21, m_background_glass);
        SWORD.setItem(22, m_background_glass);
        SWORD.setItem(23, m_background_glass);
        SWORD.setItem(24, m_background_glass);
        SWORD.setItem(25, m_background_glass);
        SWORD.setItem(26, m_background_glass);

    }

    private static void initFood() {
        FOOD = TTL.getInstance().getServer().createInventory(null, 27, TTL.getInstance().getString("language.shop.food.header"));

        ItemStack c_apple = new ItemStack(Material.APPLE, 1);
        ItemMeta c_apple_meta = c_apple.getItemMeta();
        c_apple_meta.setDisplayName(TTL.getInstance().getString("language.shop.food.apple"));
        c_apple_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 1))));
        c_apple.setItemMeta(c_apple_meta);

        ItemStack c_cake = new ItemStack(Material.CAKE, 1);
        ItemMeta c_cake_meta = c_cake.getItemMeta();
        c_cake_meta.setDisplayName(TTL.getInstance().getString("language.shop.food.cake"));
        c_cake_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 3))));
        c_cake.setItemMeta(c_cake_meta);

        ItemStack c_bread = new ItemStack(Material.BREAD, 1);
        ItemMeta c_bread_meta = c_bread.getItemMeta();
        c_bread_meta.setDisplayName(TTL.getInstance().getString("language.shop.food.bread"));
        c_bread_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 4))));
        c_bread.setItemMeta(c_bread_meta);

        ItemStack c_steak = new ItemStack(Material.COOKED_BEEF, 1);
        ItemMeta c_steak_meta = c_steak.getItemMeta();
        c_steak_meta.setDisplayName(TTL.getInstance().getString("language.shop.food.steak"));
        c_steak_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 4))));
        c_steak.setItemMeta(c_steak_meta);

        ItemStack c_gapple = new ItemStack(Material.GOLDEN_APPLE, 1);
        ItemMeta c_gapple_meta = c_gapple.getItemMeta();
        c_gapple_meta.setDisplayName(TTL.getInstance().getString("language.shop.food.g-apple"));
        c_gapple_meta.setLore(new ArrayList<>(Collections.singletonList(String.format(TTL.getInstance().getString("language.shop.price-lore"), 10))));
        c_gapple.setItemMeta(c_gapple_meta);


        FOOD.setItem(0, m_background_glass);
        FOOD.setItem(1, m_background_glass);
        FOOD.setItem(2, m_background_glass);
        FOOD.setItem(3, m_background_glass);
        FOOD.setItem(4, m_background_glass);
        FOOD.setItem(5, m_background_glass);
        FOOD.setItem(6, m_background_glass);
        FOOD.setItem(7, m_background_glass);
        FOOD.setItem(8, m_back);
        FOOD.setItem(9, m_background_glass);
        FOOD.setItem(10, m_background_glass);
        FOOD.setItem(11, c_apple);
        FOOD.setItem(12, c_cake);
        FOOD.setItem(13, c_bread);
        FOOD.setItem(14, c_steak);
        FOOD.setItem(15, c_gapple);
        FOOD.setItem(16, m_background_glass);
        FOOD.setItem(17, m_background_glass);
        FOOD.setItem(18, m_background_glass);
        FOOD.setItem(19, m_background_glass);
        FOOD.setItem(20, m_background_glass);
        FOOD.setItem(21, m_background_glass);
        FOOD.setItem(22, m_background_glass);
        FOOD.setItem(23, m_background_glass);
        FOOD.setItem(24, m_background_glass);
        FOOD.setItem(25, m_background_glass);
        FOOD.setItem(26, m_background_glass);

    }

    private static void initCategories() {
        CATEGORIES = TTL.getInstance().getServer().createInventory(null, 27, TTL.getInstance().getString("language.shop.header"));


        ItemStack c_blocks = new ItemStack(Material.WHITE_TERRACOTTA, 1);
        ItemMeta c_block_meta = c_blocks.getItemMeta();
        c_block_meta.setDisplayName(TTL.getInstance().getString("language.shop.blocks.header"));
        c_blocks.setItemMeta(c_block_meta);

        ItemStack c_sword = new ItemStack(Material.DIAMOND_SWORD, 1);
        ItemMeta c_sword_meta = c_blocks.getItemMeta();
        c_sword_meta.setDisplayName(TTL.getInstance().getString("language.shop.sword.header"));
        c_sword.setItemMeta(c_sword_meta);

        ItemStack c_bow = new ItemStack(Material.BOW, 1);
        ItemMeta c_bow_meta = c_blocks.getItemMeta();
        c_bow_meta.setDisplayName(TTL.getInstance().getString("language.shop.bow.header"));
        c_bow.setItemMeta(c_bow_meta);

        ItemStack c_armor = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
        ItemMeta c_armor_meta = c_blocks.getItemMeta();
        c_armor_meta.setDisplayName("§bRüstung");
        c_armor.setItemMeta(c_armor_meta);

        ItemStack c_food = new ItemStack(Material.BREAD, 1);
        ItemMeta c_food_meta = c_blocks.getItemMeta();
        c_food_meta.setDisplayName(TTL.getInstance().getString("language.shop.food.header"));
        c_food.setItemMeta(c_food_meta);

        ItemStack c_potion = new ItemStack(Material.SPLASH_POTION, 1);
        ItemMeta c_potion_meta = c_blocks.getItemMeta();
        c_potion_meta.setDisplayName("§cTränke");
        c_potion.setItemMeta(c_potion_meta);

        ItemStack c_special = new ItemStack(Material.BLAZE_ROD, 1);
        ItemMeta c_special_meta = c_blocks.getItemMeta();
        c_special_meta.setDisplayName("§6Special");
        c_special.setItemMeta(c_special_meta);


        CATEGORIES.setItem(0, m_background_glass);
        CATEGORIES.setItem(1, m_background_glass);
        CATEGORIES.setItem(2, m_background_glass);
        CATEGORIES.setItem(3, m_background_glass);
        CATEGORIES.setItem(4, m_background_glass);
        CATEGORIES.setItem(5, m_background_glass);
        CATEGORIES.setItem(6, m_background_glass);
        CATEGORIES.setItem(7, m_background_glass);
        CATEGORIES.setItem(8, m_close);
        CATEGORIES.setItem(9, m_background_glass);
        CATEGORIES.setItem(10, c_blocks);
        CATEGORIES.setItem(11, c_sword);
        CATEGORIES.setItem(12, c_bow);
        CATEGORIES.setItem(13, c_armor);
        CATEGORIES.setItem(14, c_food);
        CATEGORIES.setItem(15, c_potion);
        CATEGORIES.setItem(16, c_special);
        CATEGORIES.setItem(17, m_background_glass);
        CATEGORIES.setItem(18, m_background_glass);
        CATEGORIES.setItem(19, m_background_glass);
        CATEGORIES.setItem(20, m_background_glass);
        CATEGORIES.setItem(21, m_background_glass);
        CATEGORIES.setItem(22, m_background_glass);
        CATEGORIES.setItem(23, m_background_glass);
        CATEGORIES.setItem(24, m_background_glass);
        CATEGORIES.setItem(25, m_background_glass);
        CATEGORIES.setItem(26, m_background_glass);
    }
}

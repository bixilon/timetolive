package de.bixilon.minecraft.ttl.data;

import de.bixilon.minecraft.ttl.logic.CustomBlock;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Data {
    private Location m_lobby_spawn;
    private Location m_respawn_spectator;
    public HashMap<Location, CustomBlock> m_blocks = new HashMap<>();
    private final HashMap<UUID, PlayerData> m_playerdata = new HashMap<>();

    public Location getLobbyLocation() {
        return m_lobby_spawn;
    }

    public void setLobbyLocation(Location c_location) {
        m_lobby_spawn = c_location;
    }

    public Location getSpectatorLocation() {
        return m_respawn_spectator;
    }

    public void setSpectatorLocation(Location c_location) {
        m_respawn_spectator = c_location;
    }

    public PlayerData getPlayerData(UUID c_uuid) {
        return m_playerdata.get(c_uuid);
    }

    public void generatePlayerData(Player c_p) {
        m_playerdata.put(c_p.getUniqueId(), new PlayerData(c_p));
    }

    public HashMap<UUID, PlayerData> getPlayerData() {
        return m_playerdata;
    }

    public void addBlock(Location c_l) {
        m_blocks.put(c_l, new CustomBlock(c_l));
    }

    public boolean removeBlock(Location c_l) {
        if (isBlockBreakable(c_l)) {
            m_blocks.remove(c_l);
            c_l.getBlock().setType(Material.AIR);
            return true;
        }
        return false;
    }

    public boolean isBlockBreakable(Location c_l) {
        return m_blocks.containsKey(c_l);
    }

    public void breakAllBlocks() {
        List<Location> c_to_remove = new ArrayList<>();
        for (CustomBlock c_cb : m_blocks.values()) {
            c_to_remove.add(c_cb.getLocation());
            // do not remove it here, otherwise there will be an ConcurrentModificationException thrown
        }
        for (Location c_cb : c_to_remove) {
            removeBlock(c_cb);
        }
    }

    public void tickAllBlocks() {
        List<Location> c_to_remove = new ArrayList<>();
        for (CustomBlock c_cb : m_blocks.values()) {
            if (c_cb.tick()) { // maybe remove the block or at least update animation
                // remove block
                c_to_remove.add(c_cb.getLocation());
                // do not remove it here, otherwise there will be an ConcurrentModificationException thrown
            }
        }
        for (Location c_location : c_to_remove) {
            removeBlock(c_location);
        }
    }

}

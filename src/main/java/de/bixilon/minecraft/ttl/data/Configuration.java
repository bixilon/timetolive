package de.bixilon.minecraft.ttl.data;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Configuration {
    YamlConfiguration m_config;

    public Configuration(String c_filename) throws IOException {
        File c_file = new File("plugins/TTL/" + c_filename);
        if (!c_file.exists()) {
            // no configuration file
            InputStream c_input = getClass().getResourceAsStream("/config/" + c_filename);
            if (c_input == null) {
                throw new FileNotFoundException(String.format("[TTL] In the .jar file of TTL is no %s!", c_filename));
            }
            File c_folder = new File("plugins/TTL/");
            if (!c_folder.exists() && !c_folder.mkdirs()) {
                throw new IOException("[TTL] Could not create plugin folder!");
            }
            Files.copy(c_input, Paths.get(c_file.getAbsolutePath()));
            c_file = new File("plugins/TTL/" + c_filename);
        }
        m_config = YamlConfiguration.loadConfiguration(c_file);
    }

    public YamlConfiguration getConfig() {
        return m_config;
    }
}

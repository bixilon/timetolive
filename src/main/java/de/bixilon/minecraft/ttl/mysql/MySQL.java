package de.bixilon.minecraft.ttl.mysql;

import de.bixilon.minecraft.ttl.data.PlayerData;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.bukkit.entity.Player;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MySQL {
    Driver m_driver;

    public MySQL(Driver c_driver) {
        m_driver = c_driver;
    }

    public boolean isConnected() {
        return m_driver.m_connected;
    }

    public void disconnect() {
        try {
            m_driver.getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void init() {
        // load sql file
        InputStream c_is = getClass().getResourceAsStream("/sql/create.sql");
        ScriptRunner c_sr = new ScriptRunner(m_driver.getConnection());
        // disable sql output in console
        c_sr.setLogWriter(null);
        // execute sql
        c_sr.runScript(new BufferedReader(new InputStreamReader(c_is, StandardCharsets.UTF_8)));
    }

    public void join(Player c_player) {
        // update or crate player database
        PreparedStatement c_statement = m_driver.getPreparedStatement("INSERT INTO stats (uuid, name) VALUES(?, ?) ON DUPLICATE KEY UPDATE name=?");
        try {
            c_statement.setString(1, c_player.getUniqueId().toString());
            c_statement.setString(2, c_player.getName());
            c_statement.setString(3, c_player.getName());
            c_statement.executeUpdate();

            // must commit because of ?
            m_driver.getConnection().commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void saveData(PlayerData c_player) {
        // update player data
        PreparedStatement c_statement = m_driver.getPreparedStatement("UPDATE `stats` SET `games`= `games` + 1,`wins`= `wins` + ?, `kills`= `kills` + ?, `total_lifetime_gained`= `total_lifetime_gained` + ?  WHERE `uuid` = ?");
        try {
            c_statement.setInt(1, ((c_player.hasWon()) ? 1 : 0));
            c_statement.setInt(2, c_player.getKills());
            c_statement.setInt(3, c_player.getLifeTimeGained());
            c_statement.setString(4, c_player.getUUID().toString());
            c_statement.executeUpdate();

            // must commit because of ?
            m_driver.getConnection().commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

}

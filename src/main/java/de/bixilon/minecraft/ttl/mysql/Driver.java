package de.bixilon.minecraft.ttl.mysql;

import java.sql.*;

public class Driver {
    private final Connection m_connection;
    public boolean m_connected = true;

    public Driver(String c_host, int c_port, String c_database, String c_user, String c_password) {
        m_connection = connect(c_host, c_port, c_database, c_user, c_password);
        try {
            // enable auto commit
            m_connection.setAutoCommit(true);

            // select database
            getStatement().execute("use " + c_database + ";");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private Connection connect(String c_host, int c_port, String c_database, String c_user, String c_password) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://" + c_host + ":" + c_port + "/" + c_database
                    + "?autoReconnect=true&user=" + c_user + "&password=" + c_password);
        } catch (ClassNotFoundException | SQLException e) {
            m_connected = false;
            e.printStackTrace();
        }
        return null;
    }

    public Statement getStatement() {
        try {
            return m_connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Connection getConnection() {
        return m_connection;
    }

    public PreparedStatement getPreparedStatement(String c_sql) {
        try {
            return m_connection.prepareStatement(c_sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

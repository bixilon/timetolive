package de.bixilon.minecraft.ttl.commands;

import de.bixilon.minecraft.ttl.TTL;
import de.bixilon.minecraft.ttl.data.PlayerData;
import de.bixilon.minecraft.ttl.data.ShopType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Shop implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender c_s, Command c_c, String c_l, String[] c_args) {
        if (!(c_s instanceof Player)) {
            c_s.sendMessage(TTL.getInstance().getString("language.command.player-only"));
            return false;
        }
        switch (TTL.getInstance().m_status) {
            case ENDED:
            case WAITING:
                c_s.sendMessage(TTL.getInstance().getString("language.command.shop-unavailable"));
                return false;
        }
        Player p = (Player) c_s;
        PlayerData c_pd = TTL.getInstance().m_data.getPlayerData(p.getUniqueId());
        if (c_pd == null || c_pd.isDead()) {
            c_s.sendMessage(TTL.getInstance().getString("language.command.shop-unavailable"));
            return false;
        }

        p.openInventory(ShopType.CATEGORIES);
        return false;
    }

}

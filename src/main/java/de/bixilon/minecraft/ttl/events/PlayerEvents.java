package de.bixilon.minecraft.ttl.events;

import de.bixilon.minecraft.ttl.TTL;
import de.bixilon.minecraft.ttl.data.PlayerData;
import de.bixilon.minecraft.ttl.logic.GameRunnable;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.DisplaySlot;

public class PlayerEvents implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        TTL.getInstance().m_mysql.join(e.getPlayer());
        // check if server is full
        if (TTL.getInstance().getServer().getOnlinePlayers().size() == TTL.getInstance().m_config.getConfig().getInt("config.general.max-players")) {
            e.getPlayer().kickPlayer("§cDer Server ist voll!");
            return;
        }

        e.getPlayer().setGameMode(GameMode.SURVIVAL);
        // disable fly
        e.getPlayer().setAllowFlight(false);

        // clear player inventory
        e.getPlayer().getInventory().clear();
        // remove invisibility effects (if player was spectator before)
        e.getPlayer().removePotionEffect(PotionEffectType.INVISIBILITY);

        // heal player
        e.getPlayer().setHealth(20.0F);
        e.getPlayer().setFoodLevel(20);

        // teleport to area
        switch (TTL.getInstance().m_status) {
            case WAITING:
                GameRunnable.checkLobbyTimerStart();
                e.getPlayer().getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
            case ENDED:
                e.getPlayer().teleport(TTL.getInstance().m_data.getLobbyLocation());
                // allowed to join, ... set join message
                e.setJoinMessage(String.format(TTL.getInstance().getString("language.general.joined"), e.getPlayer().getName()));
                break;
            case RUNNING:
            case PROTECTION_TIME:
                TTL.getInstance().setSpectator(e.getPlayer());
                e.getPlayer().teleport(TTL.getInstance().m_data.getSpectatorLocation());
                e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
                e.setJoinMessage("");
                break;
        }
        e.getPlayer().setLevel(0);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        switch (TTL.getInstance().m_status) {
            case WAITING:
                if (TTL.getInstance().m_config.getConfig().getInt("config.lobby.players-to-start") > (TTL.getInstance().getServer().getOnlinePlayers().size() - 1)) {
                    GameRunnable.checkLobbyTimerStop();
                }
                e.setQuitMessage(String.format(TTL.getInstance().getString("language.general.left"), e.getPlayer().getName()));
                break;
            case RUNNING:
            case PROTECTION_TIME:
                // this was a game....if the player was no spectator
                if (TTL.getInstance().m_data.getPlayerData(e.getPlayer().getUniqueId()) != null && !TTL.getInstance().m_data.getPlayerData(e.getPlayer().getUniqueId()).isDead()) {
                    e.setQuitMessage(String.format(TTL.getInstance().getString("language.general.left"), e.getPlayer().getName()));
                    TTL.getInstance().m_data.getPlayerData(e.getPlayer().getUniqueId()).addGame();
                    GameRunnable.checkWin();
                } else {
                    e.setQuitMessage("");
                }
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        TTL.getInstance().setSpectator(e.getEntity());
        PlayerData c_pd = TTL.getInstance().m_data.getPlayerData(e.getEntity().getUniqueId());
        c_pd.addGame();
        // auto respawn (as spectator)
        TTL.getInstance().getServer().getScheduler().scheduleSyncDelayedTask(TTL.getInstance(), () -> {
            e.getEntity().spigot().respawn();
            if (TTL.getInstance().m_status == TTL.GameStatus.ENDED) {
                // already over
                return;
            }
            e.getEntity().addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1));
        }, 5L);


        if (c_pd.getLifeTime() == 0) {
            // no killer (ran out of lifetime)
            e.setDeathMessage(String.format(TTL.getInstance().getString("language.death.out-of-time"), c_pd.getName()));
            // check if is now over
            GameRunnable.checkWin();
            return;
        }

        if (e.getEntity().getKiller() != null) {
            // killer is a player
            PlayerData c_killer = TTL.getInstance().m_data.getPlayerData(e.getEntity().getKiller().getUniqueId());
            c_killer.addKill();
            c_killer.addTime(c_pd.getLifeTime() * TTL.getInstance().m_config.getConfig().getInt("config.times.time-from-kill") / 100);
            c_killer.addTime(TTL.getInstance().m_config.getConfig().getInt("config.times.bonus-per-kill"));
            c_killer.refreshScoreboard();
            e.setDeathMessage(String.format(TTL.getInstance().getString("language.death.killed"), c_pd.getName(), c_killer.getName()));

            // check if is now over
            GameRunnable.checkWin();
            return;
        }
        e.setDeathMessage(String.format(TTL.getInstance().getString("language.death.died"), c_pd.getName()));

        // check if is now over
        if (GameRunnable.checkWin()) {
            // yes, do not let items drop
            e.setKeepInventory(true);
            e.getEntity().getInventory().clear();
        }
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
        e.setRespawnLocation(TTL.getInstance().m_data.getSpectatorLocation());
    }
}

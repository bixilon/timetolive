package de.bixilon.minecraft.ttl.events;

import de.bixilon.minecraft.ttl.TTL;
import de.bixilon.minecraft.ttl.data.PlayerData;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.*;

public class ProtectEvent implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e) {
        switch (TTL.getInstance().m_status) {
            case RUNNING:
            case PROTECTION_TIME:
                PlayerData c_data = TTL.getInstance().m_data.getPlayerData(e.getPlayer().getUniqueId());
                if (c_data == null || c_data.isDead()) {
                    e.setCancelled(true);
                    return;
                }
                TTL.getInstance().m_data.addBlock(e.getBlock().getLocation());
                break;
            case ENDED:
            case WAITING:
                e.setCancelled(true);
                break;
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent e) {
        switch (TTL.getInstance().m_status) {
            case RUNNING:
            case PROTECTION_TIME:
                PlayerData c_data = TTL.getInstance().m_data.getPlayerData(e.getPlayer().getUniqueId());
                if (c_data == null || c_data.isDead()) {
                    e.setCancelled(true);
                    return;
                }

                if (!TTL.getInstance().m_data.removeBlock(e.getBlock().getLocation())) {
                    e.setCancelled(true);
                    return;
                }
                break;
            case ENDED:
            case WAITING:
                e.setCancelled(true);
                break;
        }
    }

    @EventHandler
    public void onBlockBurn(BlockBurnEvent e) {
        switch (TTL.getInstance().m_status) {
            case ENDED:
            case RUNNING:
                e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockGrow(BlockGrowEvent e) {
        switch (TTL.getInstance().m_status) {
            case ENDED:
            case WAITING:
                e.setCancelled(true);
        }
    }

    @EventHandler
    public void onInteract(EntityInteractEvent e) {
        switch (TTL.getInstance().m_status) {
            case RUNNING:
            case PROTECTION_TIME:
                if (e.getEntity() instanceof Player) {
                    Player c_player = (Player) e.getEntity();
                    PlayerData c_data = TTL.getInstance().m_data.getPlayerData(c_player.getUniqueId());
                    if (c_data == null || c_data.isDead()) {
                        e.setCancelled(true);
                    }
                }
                break;
            case ENDED:
            case WAITING:
                e.setCancelled(true);
                break;
        }
    }

    @EventHandler
    public void onDamage(EntityDamageEvent e) {
        switch (TTL.getInstance().m_status) {
            case ENDED:
            case WAITING:
            case PROTECTION_TIME:
                e.setCancelled(true);
                break;
        }
    }

    @EventHandler
    public void onDamage(EntityDamageByEntityEvent e) {
        switch (TTL.getInstance().m_status) {
            case RUNNING:
            case PROTECTION_TIME:
                if (e.getDamager() instanceof Player) {
                    Player c_damager = (Player) e.getDamager();
                    PlayerData c_data_damager = TTL.getInstance().m_data.getPlayerData(c_damager.getUniqueId());
                    if (c_data_damager == null || c_data_damager.isDead()) {
                        e.setCancelled(true);
                    }
                }
                break;
            case ENDED:
            case WAITING:
                e.setCancelled(true);
        }
    }

    @EventHandler
    public void onItemPickup(EntityPickupItemEvent e) {
        switch (TTL.getInstance().m_status) {
            case RUNNING:
            case PROTECTION_TIME:
                if (e.getEntity() instanceof Player) {
                    Player c_damager = (Player) e.getEntity();
                    PlayerData c_data_damager = TTL.getInstance().m_data.getPlayerData(c_damager.getUniqueId());
                    if (c_data_damager == null || c_data_damager.isDead()) {
                        e.setCancelled(true);
                    }
                }
                break;
            case ENDED:
            case WAITING:
                e.setCancelled(true);
        }
    }

    @EventHandler
    public void onItemDrop(EntityDropItemEvent e) {
        switch (TTL.getInstance().m_status) {
            case RUNNING:
            case PROTECTION_TIME:
                if (e.getEntity() instanceof Player) {
                    Player c_damager = (Player) e.getEntity();
                    PlayerData c_data_damager = TTL.getInstance().m_data.getPlayerData(c_damager.getUniqueId());
                    if (c_data_damager == null || c_data_damager.isDead()) {
                        e.setCancelled(true);
                    }
                }
                break;
            case ENDED:
            case WAITING:
                e.setCancelled(true);
        }
    }

}


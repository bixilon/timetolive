package de.bixilon.minecraft.ttl.events;

import de.bixilon.minecraft.ttl.TTL;
import de.bixilon.minecraft.ttl.data.ShopType;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;

public class ShopEvents implements Listener {

    public static boolean buy(Player c_p, int c_seconds) {
        if (!TTL.getInstance().m_data.getPlayerData(c_p.getUniqueId()).buy(c_seconds)) {
            c_p.sendMessage(TTL.getInstance().getString("language.shop.not-enough-time"));
            return false;
        }
        c_p.playSound(c_p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1.0F, 1.0F);
        return true;
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (!(e.getWhoClicked() instanceof Player) || e.getCurrentItem() == null) {
            return;
        }
        Player p = (Player) e.getWhoClicked();
        if (e.getInventory() == ShopType.CATEGORIES) {
            switch (e.getSlot()) {
                case 8:
                    p.closeInventory();
                    break;
                case 10:
                    p.openInventory(ShopType.BLOCKS);
                    break;
                case 11:
                    p.openInventory(ShopType.SWORD);
                    break;
                case 12:
                    p.openInventory(ShopType.BOW);
                    break;
                case 13:
                    p.openInventory(ShopType.ARMOR);
                    break;
                case 14:
                    p.openInventory(ShopType.FOOD);
                    break;
                case 15:
                    p.openInventory(ShopType.POTIONS);
                    break;
                case 16:
                    p.openInventory(ShopType.SPECIAL);
                    break;
                default:
                    if (Objects.requireNonNull(e.getCurrentItem()).equals(ShopType.m_background_glass)) {
                        e.setCancelled(true);
                    } else {
                        return;
                    }
            }
        } else if (e.getInventory() == ShopType.FOOD) {
            switch (e.getSlot()) {
                case 8:
                    p.openInventory(ShopType.CATEGORIES);
                    break;
                case 11:
                    if (buy(p, 1)) {
                        p.getInventory().addItem(new ItemStack(Material.APPLE, 1));
                    }
                    break;
                case 12:
                    if (buy(p, 3)) {
                        p.getInventory().addItem(new ItemStack(Material.CAKE, 1));
                    }
                    break;
                case 13:
                    if (buy(p, 4)) {
                        p.getInventory().addItem(new ItemStack(Material.BREAD, 1));
                    }
                    break;
                case 14:
                    if (buy(p, 4)) {
                        p.getInventory().addItem(new ItemStack(Material.COOKED_BEEF, 1));
                    }
                    break;
                case 15:
                    if (buy(p, 10)) {
                        p.getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE, 1));
                    }
                    break;
                default:
                    if (Objects.requireNonNull(e.getCurrentItem()).equals(ShopType.m_background_glass)) {
                        e.setCancelled(true);
                    } else {
                        return;
                    }
            }
        } else if (e.getInventory() == ShopType.SWORD) {
            switch (e.getSlot()) {
                case 8:
                    p.openInventory(ShopType.CATEGORIES);
                    break;
                case 11:
                    if (buy(p, 10)) {
                        ItemStack c_wood_sword = new ItemStack(Material.WOODEN_SWORD, 1);
                        ItemMeta c_wood_sword_meta = c_wood_sword.getItemMeta();
                        c_wood_sword.setItemMeta(c_wood_sword_meta);
                        c_wood_sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
                        p.getInventory().addItem(c_wood_sword);
                    }
                    break;
                case 12:
                    if (buy(p, 20)) {
                        ItemStack c_stone_sword = new ItemStack(Material.STONE_SWORD, 1);
                        ItemMeta c_stone_sword_meta = c_stone_sword.getItemMeta();
                        c_stone_sword.setItemMeta(c_stone_sword_meta);
                        c_stone_sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
                        p.getInventory().addItem(c_stone_sword);
                    }
                    break;
                case 13:
                    if (buy(p, 50)) {
                        ItemStack c_iron_sword = new ItemStack(Material.IRON_SWORD, 1);
                        ItemMeta c_iron_sword_meta = c_iron_sword.getItemMeta();
                        c_iron_sword.setItemMeta(c_iron_sword_meta);
                        c_iron_sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
                        p.getInventory().addItem(c_iron_sword);
                    }
                    break;
                case 14:
                    if (buy(p, 100)) {
                        ItemStack c_diamond_sword = new ItemStack(Material.DIAMOND_SWORD, 1);
                        ItemMeta c_diamond_sword_meta = c_diamond_sword.getItemMeta();
                        c_diamond_sword.setItemMeta(c_diamond_sword_meta);
                        c_diamond_sword.addEnchantment(Enchantment.DAMAGE_ALL, 1);
                        p.getInventory().addItem(c_diamond_sword);
                    }
                    break;
                case 15:
                    if (buy(p, 150)) {
                        ItemStack c_diamond_sword_fire = new ItemStack(Material.DIAMOND_SWORD, 1);
                        ItemMeta c_diamond_sword_fire_meta = c_diamond_sword_fire.getItemMeta();
                        c_diamond_sword_fire.setItemMeta(c_diamond_sword_fire_meta);
                        c_diamond_sword_fire.addEnchantment(Enchantment.DAMAGE_ALL, 1);
                        c_diamond_sword_fire.addEnchantment(Enchantment.FIRE_ASPECT, 1);
                        p.getInventory().addItem(c_diamond_sword_fire);
                    }
                    break;
                default:
                    if (Objects.requireNonNull(e.getCurrentItem()).equals(ShopType.m_background_glass)) {
                        e.setCancelled(true);
                    } else {
                        return;
                    }
            }
        } else if (e.getInventory() == ShopType.BOW) {
            switch (e.getSlot()) {
                case 8:
                    p.openInventory(ShopType.CATEGORIES);
                    break;
                case 10:
                    if (buy(p, 10)) {
                        ItemStack c_bow_1 = new ItemStack(Material.BOW, 1);
                        p.getInventory().addItem(c_bow_1);
                    }
                    break;
                case 11:
                    if (buy(p, 20)) {
                        ItemStack c_bow_2 = new ItemStack(Material.BOW, 1);
                        c_bow_2.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
                        p.getInventory().addItem(c_bow_2);
                    }
                    break;
                case 12:
                    if (buy(p, 75)) {
                        ItemStack c_bow_3 = new ItemStack(Material.BOW, 1);
                        c_bow_3.addEnchantment(Enchantment.ARROW_DAMAGE, 3);
                        p.getInventory().addItem(c_bow_3);
                    }
                    break;
                case 13:
                    if (buy(p, 75)) {
                        ItemStack c_bow_3_infinity = new ItemStack(Material.BOW, 1);
                        c_bow_3_infinity.addEnchantment(Enchantment.ARROW_DAMAGE, 3);
                        c_bow_3_infinity.addEnchantment(Enchantment.ARROW_INFINITE, 1);
                        p.getInventory().addItem(c_bow_3_infinity);
                    }
                    break;
                case 14:
                    if (buy(p, 150)) {
                        ItemStack c_bow_4 = new ItemStack(Material.BOW, 1);
                        c_bow_4.addEnchantment(Enchantment.ARROW_DAMAGE, 5);
                        c_bow_4.addEnchantment(Enchantment.ARROW_FIRE, 1);
                        p.getInventory().addItem(c_bow_4);
                    }
                    break;
                case 15:
                    if (buy(p, 200)) {
                        ItemStack c_bow_5 = new ItemStack(Material.BOW, 1);
                        c_bow_5.addEnchantment(Enchantment.ARROW_DAMAGE, 5);
                        c_bow_5.addEnchantment(Enchantment.ARROW_FIRE, 1);
                        c_bow_5.addEnchantment(Enchantment.ARROW_INFINITE, 1);

                        p.getInventory().addItem(c_bow_5);
                    }
                    break;
                case 16:
                    if (buy(p, 10)) {
                        ItemStack c_arrow = new ItemStack(Material.ARROW, 8);
                        p.getInventory().addItem(c_arrow);
                    }
                    break;
                default:
                    if (Objects.requireNonNull(e.getCurrentItem()).equals(ShopType.m_background_glass)) {
                        e.setCancelled(true);
                    } else {
                        return;
                    }
            }

        } else if (e.getInventory() == ShopType.BLOCKS) {
            switch (e.getSlot()) {
                case 8:
                    p.openInventory(ShopType.CATEGORIES);
                    break;
                case 12:
                    if (buy(p, 8)) {
                        ItemStack c_glass = new ItemStack(Material.GLASS, 16);
                        p.getInventory().addItem(c_glass);
                    }
                    break;
                case 13:
                    if (buy(p, 10)) {
                        ItemStack c_wool = new ItemStack(Material.WHITE_WOOL, 16);
                        p.getInventory().addItem(c_wool);
                    }
                    break;
                case 14:
                    if (buy(p, 16)) {
                        ItemStack c_obsidian = new ItemStack(Material.OBSIDIAN, 16);
                        p.getInventory().addItem(c_obsidian);
                    }
                    break;
                default:
                    if (Objects.requireNonNull(e.getCurrentItem()).equals(ShopType.m_background_glass)) {
                        e.setCancelled(true);
                    } else {
                        return;
                    }
            }

        } else {
            return;
        }
        e.setCancelled(true);
    }
}
